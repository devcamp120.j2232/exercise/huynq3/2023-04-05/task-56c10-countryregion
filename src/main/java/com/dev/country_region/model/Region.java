package com.dev.country_region.model;

public class Region {
    private String regionCode;
    private String regionName;
    
    public Region() {
    }

    public Region(String regionCode, String regionName) {
        this.regionCode = regionCode;
        this.regionName = regionName;
    }
    
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionCode() {
        return regionCode;
    }
    
    public String getRegionName() {
        return regionName;
    }
}
