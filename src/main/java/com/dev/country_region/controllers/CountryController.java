package com.dev.country_region.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dev.country_region.Service.CountryService;
import com.dev.country_region.model.Country;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public ArrayList<Country> getAllCountries(){
        ArrayList<Country> allCountry= countryService.getAllCountries();
        return allCountry;
    }
    @GetMapping("/country-info")
    public Country getCountryInfo(@RequestParam(name="code", required=true) String countryCode){
        ArrayList<Country> allCountry= countryService.getAllCountries();
        Country foundCountry= new Country();

        for (Country country : allCountry) {
            if (country.getCountryCode().equals(countryCode)){
                foundCountry=country;
            }
        }
        return foundCountry;
    }



}
