package com.dev.country_region.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dev.country_region.*;
import com.dev.country_region.Service.RegionService;
import com.dev.country_region.model.Region;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RegionController {
    
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info")
    public Region getRegionInfo(@RequestParam(name="code", required = true) String regionCode){
        Region foundRegion= regionService.filterRegions(regionCode);
        return foundRegion;
    }
}
