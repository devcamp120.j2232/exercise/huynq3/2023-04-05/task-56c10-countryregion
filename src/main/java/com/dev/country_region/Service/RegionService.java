package com.dev.country_region.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.dev.country_region.model.Region;

@Service
public class RegionService {
    Region hanoi = new Region("HN", "Ha Noi");
    Region saigon = new Region("SG", "Sai Gon");
    Region danang = new Region("DN", "Da Nang");

    Region sydney = new Region("SYD", "Sydney");
    Region melbourn = new Region("MEL", "Melbourn");
    Region perth = new Region("PER", "Perth");

    Region newyorks = new Region("NY", "New Yorks");
    Region losangeles = new Region("LA", "Los Angeles");
    Region texas = new Region("TX", "Texas");

    public ArrayList<Region> getRegionUS() {
        ArrayList<Region> regionsUS = new ArrayList<>();
        regionsUS.add(newyorks);
        regionsUS.add(losangeles);
        regionsUS.add(texas);
        return regionsUS;
    }

    public ArrayList<Region> getRegionAustralia() {
        ArrayList<Region> regionsAustralia = new ArrayList<>();
        regionsAustralia.add(sydney);
        regionsAustralia.add(melbourn);
        regionsAustralia.add(perth);
        return regionsAustralia;
    }

    public ArrayList<Region> getRegionVietnam() {
        ArrayList<Region> regionsVietNam = new ArrayList<>();
        regionsVietNam.add(hanoi);
        regionsVietNam.add(saigon);
        regionsVietNam.add(danang);
        return regionsVietNam;
    }
    //C1
    // public ArrayList<Region> getAllRegions() {
    //     ArrayList<Region> regions = new ArrayList<>();
    //     regions.add(hanoi);
    //     regions.add(saigon);
    //     regions.add(danang);
    //     regions.add(sydney);
    //     regions.add(melbourn);
    //     regions.add(perth);
    //     regions.add(newyorks);
    //     regions.add(losangeles);
    //     regions.add(texas);


    //     return regions;
    // }

    //C2 filter trong service
    public Region filterRegions(String regionCode) {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(hanoi);
        regions.add(saigon);
        regions.add(danang);
        regions.add(sydney);
        regions.add(melbourn);
        regions.add(perth);
        regions.add(newyorks);
        regions.add(losangeles);
        regions.add(texas);

        Region foundRegion= new Region();

            for (Region regionElement : regions) {
                if (regionElement.getRegionCode().equals(regionCode)){
                    foundRegion=regionElement;
                }
            }

        return foundRegion;
    }

}
