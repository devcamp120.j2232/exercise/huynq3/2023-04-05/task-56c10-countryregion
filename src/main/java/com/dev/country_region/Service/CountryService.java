package com.dev.country_region.Service;
import com.dev.country_region.Service.CountryService;
import com.dev.country_region.model.Country;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryService {
    @Autowired
    public RegionService regionService;
    Country vietnam = new Country("VN", "Viet Nam");
    Country us = new Country("US", "My");
    Country aus = new Country("AUS", "Uc");

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountries = new ArrayList<>();
        allCountries.add(vietnam);
        allCountries.add(us);
        allCountries.add(aus);
        vietnam.setRegions(regionService.getRegionVietnam());
        us.setRegions(regionService.getRegionUS());
        aus.setRegions(regionService.getRegionAustralia());

        return allCountries;
    }
}
